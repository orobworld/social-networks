package socialnet;

import ilog.concert.IloException;
import ilog.cplex.IloCplex.Status;
import java.util.Set;
import models.AssignmentModel;
import models.DistanceModel;
import models.DistanceModel2;
import models.DistanceModel3;
import models.FlowModel;

/**
 * SocialNet tests various models for a social network-based optimization
 * problem posed on OR Stack Exchange.
 *
 * Link: https://or.stackexchange.com/questions/8674/is-this-ilp-formulation-
 * for-group-closeness-centrality-a-column-generation-appro
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class SocialNet {

  /**
   * Dummy constructor.
   */
  private SocialNet() { }

  /**
   * Runs the experiments.
   * @param args the command line arguments (unused)
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(final String[] args) {
    // Parameters for the test graph.
    int nVertices = 2000;   // vertex count
    int nEdges = 10000;     // target edge count (actual may be higher)
    int nSelected = 20;     // size of the group of selected vertices
    long seed = 220714;     // random number seed
    int prog = 1000;        // frequency of progress reports
    // Generate a graph instance.
    Graph graph = new Graph(nVertices, nEdges, seed, prog);
    System.out.println(graph);
    // Set a time limit for each run (in seconds).
    double timeLimit = 600;
    // Try the flow model.
    System.out.println("\n*** Trying flow model. ***\n");
    try (FlowModel fm = new FlowModel(graph, nSelected)) {
      Status status = fm.solve(timeLimit);
      System.out.println("\nFinal solver status = " + status
                         + "\nFinal objective value = " + fm.getObjValue());
      // Check and print the solution.
      Set<Integer> sol = fm.getSolution();
      System.out.println(graph.check(sol));
    } catch (IloException ex) {
      System.out.println("The flow model blew up:\n" + ex.getMessage());
    }
    // Try the original distance model.
    System.out.println("\n*** Trying original distance model. ***\n");
    try (DistanceModel dm = new DistanceModel(graph, nSelected)) {
      Status status = dm.solve(timeLimit);
      System.out.println("\nFinal solver status = " + status
                         + "\nFinal objective value = " + dm.getObjValue());
      // Check and print the solution.
      Set<Integer> sol = dm.getSolution();
      System.out.println(graph.check(sol));
    } catch (IloException ex) {
      System.out.println("The distance model blew up:\n" + ex.getMessage());
    }
    // Try the distance model with Rob's modification.
    System.out.println("\n*** Trying modified distance model. ***\n");
    try (DistanceModel2 dm = new DistanceModel2(graph, nSelected)) {
      Status status = dm.solve(timeLimit);
      System.out.println("\nFinal solver status = " + status
                         + "\nFinal objective value = " + dm.getObjValue());
      // Check and print the solution.
      Set<Integer> sol = dm.getSolution();
      System.out.println(graph.check(sol));
    } catch (IloException ex) {
      System.out.println("The distance model blew up:\n" + ex.getMessage());
    }
    // Try the distance model with a mix of binary and continuous variables.
    System.out.println("\n*** Trying mixed integer distance model. ***\n");
    try (DistanceModel3 dm = new DistanceModel3(graph, nSelected)) {
      Status status = dm.solve(timeLimit);
      System.out.println("\nFinal solver status = " + status
                         + "\nFinal objective value = " + dm.getObjValue());
      // Check and print the solution.
      Set<Integer> sol = dm.getSolution();
      System.out.println(graph.check(sol));
    } catch (IloException ex) {
      System.out.println("The distance model blew up:\n" + ex.getMessage());
    }
    // Try the assignment model.
    System.out.println("\n*** Trying assignment model. ***\n");
    try (AssignmentModel am = new AssignmentModel(graph, nSelected, prog)) {
      Status status = am.solve(timeLimit);
      System.out.println("\nFinal solver status = " + status
                         + "\nFinal objective value = " + am.getObjValue());
      // Check and print the solution.
      Set<Integer> sol = am.getSolution();
      System.out.println(graph.check(sol));
    } catch (IloException ex) {
      System.out.println("The assignment model blew up:\n" + ex.getMessage());
    }
  }

}
