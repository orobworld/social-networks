# Social Network Optimization Problem #

### What is this repository for? ###

This code relates to a [problem](https://or.stackexchange.com/questions/8674/is-this-ilp-formulation-for-group-closeness-centrality-a-column-generation-appro) posted on OR Stack Exchange, in which as subset of nodes with a specified cardinality is to be chosen within a graph such that the total distance from all nodes to their nearest selected counterparts is minimized. The code, which was developed using CPLEX 22.1 and has no external dependencies other than CPLEX, lets the user generate a random graph and then test five different integer or mixed-integer linear programming models. Details of the model can be found in my [blog post](https://orinanobworld.blogspot.com/2022/07/models-for-social-network-problem.html) on the subject.

### License ###
The code here is copyrighted by Paul A. Rubin (yours truly) and licensed under a [Creative Commons Attribution 3.0 Unported License](http://creativecommons.org/licenses/by/3.0/deed.en_US). 

